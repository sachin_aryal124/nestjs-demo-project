import * as crypto from 'node:crypto'

import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { JwtService } from '@nestjs/jwt'
import { MailerService } from '@nestjs-modules/mailer'
import type { User } from '@prisma/client'
import * as argon2 from 'argon2'

import type { CreateUserDto } from '../users/dtos/create-user-dto'
import { UsersService } from '../users/users.service'
import type { ForgotPasswordDto } from './dtos/forgot-password.dto'
import { RefreshTokenService } from './refresh-token.service'

export interface AuthToken {
  accessToken: string
  refreshToken: string
}

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private readonly mailerService: MailerService,
    private readonly configService: ConfigService,
    private readonly refreshTokenService: RefreshTokenService,
  ) {}
  public async validateUserWithoutPassword(
    email: string,
  ): Promise<User | null> {
    return this.usersService.findOne(email)
  }
  public async validateUser(
    email: string,
    password: string,
  ): Promise<User | null> {
    const user = await this.usersService.findOne(email)
    if (!user) return null
    const isPasswordMatch = await argon2.verify(user.password, password)
    if (!isPasswordMatch) return null
    return user
  }
  public async generateTokens(user: User): Promise<AuthToken> {
    const payload = { id: user.id, sub: user.email }
    const refreshToken = await this.jwtService.signAsync(payload, {
      secret: this.configService.get<string>('JWT_REFRESH_TOKEN_SECRET'),
      expiresIn: this.configService.get<string>('JWT_REFRESH_TOKEN_EXPIRES_IN'),
    })
    const accessToken = await this.jwtService.signAsync(payload, {
      secret: this.configService.get<string>('JWT_ACCESS_TOKEN_SECRET'),
      expiresIn: this.configService.get<string>('JWT_ACCESS_TOKEN_EXPIRES_IN'),
    })
    await this.refreshTokenService.create(refreshToken, user)
    return { accessToken, refreshToken }
  }
  public async login(user: User): Promise<AuthToken> {
    const tokens = await this.generateTokens(user)
    return tokens
  }
  public async register(createUserDto: CreateUserDto): Promise<AuthToken> {
    const user = await this.usersService.create(createUserDto)
    const tokens = await this.generateTokens(user)
    return tokens
  }
  public async forgotPassword(
    forgotPasswordDto: ForgotPasswordDto,
  ): Promise<{ message: string }> {
    const { email } = forgotPasswordDto
    const user = await this.usersService.findOne(email)
    if (!user) return { message: 'User with this email does not exist' }
    const hasAlreadyRequestedToken =
      user.passwordResetToken && user.passwordResetTokenExpiresAt
        ? user.passwordResetTokenExpiresAt > new Date()
        : false
    if (hasAlreadyRequestedToken)
      return {
        message:
          'You have already requested a password reset token.Please check your email or request a new token after 1 hour',
      }
    const passwordResetToken = crypto.randomBytes(32).toString('hex')
    const ONE_HOUR_IN_MILLISECONDS = 60 * 60 * 1000
    const passwordResetTokenExpiresAt = new Date(
      Date.now() + ONE_HOUR_IN_MILLISECONDS,
    )
    await this.usersService.update(email, {
      passwordResetToken,
      passwordResetTokenExpiresAt,
    })
    await this.mailerService.sendMail({
      to: email,
      subject: 'Password Reset Token',
      text: `Your password reset token is ${passwordResetToken}`,
    })
    return {
      message:
        'Please check your email for password reset token.The token will be valid for only 1 hour',
    }
  }
  public async logout(refreshToken: string, user: User) {
    await this.refreshTokenService.delete(refreshToken, user)
    return { message: 'Successfully logged out' }
  }
  public async deleteAccount(user: User) {
    return this.usersService.update(user.email, { deletedAt: new Date() })
  }
}
