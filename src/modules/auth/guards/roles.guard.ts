import type { CanActivate, ExecutionContext } from '@nestjs/common'
import { Injectable } from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import type { UserRole } from '@prisma/client'

import { ROLES_METADATA_NAME } from '../../../core/constants/strings'

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}
  canActivate(context: ExecutionContext): boolean {
    const { user } = context.switchToHttp().getRequest()
    const roles = this.reflector.get<UserRole[]>(
      ROLES_METADATA_NAME,
      context.getHandler(),
    )
    if (!roles) return true
    if (user && roles.includes(user.role)) return true
    return false
  }
}
