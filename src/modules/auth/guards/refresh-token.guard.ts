import { Injectable } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'

import { REFRESH_TOKEN_GUARD_NAME } from '../../../core/constants/strings'

@Injectable()
export class RefreshTokenGuard extends AuthGuard(REFRESH_TOKEN_GUARD_NAME) {}
