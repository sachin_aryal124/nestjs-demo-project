import type { ExecutionContext } from '@nestjs/common'
import { createParamDecorator } from '@nestjs/common'
import type { User } from '@prisma/client'

export const GetUser = createParamDecorator<User>(
  (_data: unknown, context: ExecutionContext) => {
    const request = context.switchToHttp().getRequest()
    const { password: _password, ...rest } = request.user
    return rest
  },
)
