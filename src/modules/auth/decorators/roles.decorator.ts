import { SetMetadata } from '@nestjs/common'
import type { UserRole } from '@prisma/client'

import { ROLES_METADATA_NAME } from '../../../core/constants/strings'

export const Roles = (...roles: UserRole[]) =>
  SetMetadata(ROLES_METADATA_NAME, roles)
