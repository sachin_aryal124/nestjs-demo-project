import { Injectable, UnauthorizedException } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { PassportStrategy } from '@nestjs/passport'
import type { Request } from 'express'
import { ExtractJwt, Strategy } from 'passport-jwt'

import {
  ACCESS_TOKEN_COOKIE_NAME,
  REFRESH_TOKEN_COOKIE_NAME,
  REFRESH_TOKEN_GUARD_NAME,
} from '../../../core/constants/strings'
import { extractJwtFromCookie } from '../../../core/utils/jwt'
import { AuthService } from '../auth.service'
import { RefreshTokenService } from '../refresh-token.service'
import type { DecodedJwtPayload } from './jwt.strategy'

@Injectable()
export class RefreshTokenStrategy extends PassportStrategy(
  Strategy,
  REFRESH_TOKEN_GUARD_NAME,
) {
  constructor(
    private readonly configService: ConfigService,
    private readonly authService: AuthService,
    private readonly refreshTokenService: RefreshTokenService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        ExtractJwt.fromAuthHeaderAsBearerToken(),
        extractJwtFromCookie(ACCESS_TOKEN_COOKIE_NAME),
      ]),
      secretOrKey: configService.get<string>('JWT_REFRESH_TOKEN_SECRET'),
      passReqToCallback: true,
    })
  }
  async validate(request: Request, payload: DecodedJwtPayload) {
    const user = await this.authService.validateUserWithoutPassword(payload.sub)
    if (!user) throw new UnauthorizedException()
    const refreshToken = extractJwtFromCookie(REFRESH_TOKEN_COOKIE_NAME)(
      request,
    )
    if (!refreshToken) throw new UnauthorizedException()
    const validatedRefreshToken = await this.refreshTokenService.validate(
      refreshToken,
    )
    if (!validatedRefreshToken) throw new UnauthorizedException()
    const tokens = await this.authService.generateTokens(user)
    return tokens
  }
}
