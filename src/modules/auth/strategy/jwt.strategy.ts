import { Injectable, UnauthorizedException } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { PassportStrategy } from '@nestjs/passport'
import type { User } from '@prisma/client'
import { ExtractJwt, Strategy } from 'passport-jwt'

import { ACCESS_TOKEN_COOKIE_NAME } from '../../../core/constants/strings'
import { extractJwtFromCookie } from '../../../core/utils/jwt'
import { UsersService } from '../../users/users.service'

export interface DecodedJwtPayload {
  id: string
  sub: string
  iat: number
  exp: number
}

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly configService: ConfigService,
    private readonly usersService: UsersService,
  ) {
    super({
      secretOrKey: configService.get<string>('JWT_ACCESS_TOKEN_SECRET'),
      jwtFromRequest: ExtractJwt.fromExtractors([
        extractJwtFromCookie(ACCESS_TOKEN_COOKIE_NAME),
        ExtractJwt.fromAuthHeaderAsBearerToken(),
      ]),
      ignoreExpiration: false,
    })
  }
  async validate(payload: DecodedJwtPayload): Promise<User> {
    console.log('payload', payload)
    const tokenExpiresIn = new Date(payload.exp * 1000)
    const now = new Date()
    if (tokenExpiresIn < now) throw new UnauthorizedException()
    const user = await this.usersService.findOne(payload.sub)
    if (!user) throw new UnauthorizedException()
    if (user.deletedAt !== null) throw new UnauthorizedException()
    return user
  }
}
