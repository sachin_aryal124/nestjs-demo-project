import { Injectable } from '@nestjs/common'
import type { RefreshToken, User } from '@prisma/client'

import { PrismaService } from '../../core/services/prisma.service'

@Injectable()
export class RefreshTokenService {
  constructor(private readonly prismaService: PrismaService) {}

  public async validate(refreshToken: string): Promise<RefreshToken | null> {
    const refreshTokenDB = await this.prismaService.refreshToken.findUnique({
      where: { token: refreshToken },
    })
    if (!refreshTokenDB) return null
    const isRefreshTokenExpired = refreshTokenDB.expiresAt < new Date()
    if (isRefreshTokenExpired) return null
    return refreshTokenDB
  }
  public async create(
    refreshToken: string,
    user: User,
    defaultExpiresAt?: Date,
  ): Promise<RefreshToken> {
    const ONE_MONTH_IN_MILLISECONDS = 30 * 24 * 60 * 60 * 1000
    const expiresAt = new Date(Date.now() + ONE_MONTH_IN_MILLISECONDS)
    const refreshTokenExpiresAt = defaultExpiresAt ?? expiresAt
    const newRefreshToken = await this.prismaService.refreshToken.create({
      data: {
        token: refreshToken,
        expiresAt: refreshTokenExpiresAt,
        userId: user.id,
      },
    })
    return newRefreshToken
  }
  public async delete(refreshToken: string, user: User) {
    return this.prismaService.refreshToken.deleteMany({
      where: { token: refreshToken, userId: user.id },
    })
  }
}
