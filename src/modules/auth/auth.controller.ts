import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Post,
  Request,
  Response,
  UseGuards,
} from '@nestjs/common'
import { User } from '@prisma/client'
import { Request as RequestType, Response as ResponseType } from 'express'

import { REFRESH_TOKEN_COOKIE_NAME } from '../../core/constants/strings'
import {
  clearAccessAndRefreshTokenCookie,
  setAccessAndRefreshTokenCookie,
} from '../../core/utils/cookies'
import { extractJwtFromCookie } from '../../core/utils/jwt'
import { CreateUserDto } from '../users/dtos/create-user-dto'
import type { AuthToken } from './auth.service'
import { AuthService } from './auth.service'
import { GetUser } from './decorators/get-user.decorator'
import { ForgotPasswordDto } from './dtos/forgot-password.dto'
import { JwtAuthGuard } from './guards/jwt-auth.guard'
import { LocalAuthGuard } from './guards/local-auth.guard'
import { RefreshTokenGuard } from './guards/refresh-token.guard'

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  @UseGuards(LocalAuthGuard)
  public async login(
    @GetUser() user: User,
    @Response({ passthrough: true }) response: ResponseType,
  ) {
    const tokens = await this.authService.login(user)
    setAccessAndRefreshTokenCookie(tokens)(response)
    return {
      message: 'Logged in successfully',
      accessToken: tokens.accessToken,
      refreshToken: tokens.refreshToken,
    }
  }
  @Post('register')
  public async register(
    @Body() createUserDto: CreateUserDto,
    @Response({ passthrough: true }) response: ResponseType,
  ) {
    try {
      const tokens = await this.authService.register(createUserDto)
      setAccessAndRefreshTokenCookie(tokens)(response)
      return {
        message: 'Registered successfully',
        accessToken: tokens.accessToken,
        refreshToken: tokens.refreshToken,
      }
    } catch (error) {
      if (error.code === 'P2002') {
        throw new HttpException('Email already exists', HttpStatus.BAD_REQUEST)
      }
      throw error
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get('logout')
  public async logout(
    @Response({ passthrough: true }) response: ResponseType,
    @Request() request: RequestType,
    @GetUser() user: User,
  ) {
    const refreshToken = extractJwtFromCookie(REFRESH_TOKEN_COOKIE_NAME)(
      request,
    )
    if (!refreshToken)
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED)
    await this.authService.logout(refreshToken, user)
    clearAccessAndRefreshTokenCookie(response)
    return { message: 'Logged out successfully' }
  }
  @Post('forgot-password')
  public async forgotPassword(@Body() forgotPasswordDto: ForgotPasswordDto) {
    return this.authService.forgotPassword(forgotPasswordDto)
  }

  @UseGuards(JwtAuthGuard)
  @Post('delete-account')
  public async deleteAccount(@GetUser() user: User) {
    return this.authService.deleteAccount(user)
  }

  @Post('refresh-token')
  @UseGuards(RefreshTokenGuard)
  public async refreshToken(
    @Request() request: RequestType,
    @Response({ passthrough: true }) response: ResponseType,
  ) {
    const tokens = request.user as AuthToken
    setAccessAndRefreshTokenCookie(tokens)(response)
    setAccessAndRefreshTokenCookie(tokens)(response)
    return {
      message: 'Refreshed token successfully',
      accessToken: tokens.accessToken,
      refreshToken: tokens.refreshToken,
    }
  }
}
