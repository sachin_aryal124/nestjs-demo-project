import { Module } from '@nestjs/common'

import { PrismaService } from '../../core/services/prisma.service'
import { UploadImageService } from '../upload-image/upload-image.service'
import { ProductsController } from './products.controller'
import { ProductsService } from './products.service'

@Module({
  controllers: [ProductsController],
  providers: [ProductsService, PrismaService, UploadImageService],
})
export class ProductsModule {}
