import type { Prisma } from '@prisma/client'
import { Type } from 'class-transformer'
import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsPositive,
  IsString,
  MaxLength,
} from 'class-validator'

export class CreateProductDto
  implements Omit<Prisma.ProductCreateInput, 'image'>
{
  @IsString()
  @IsNotEmpty()
  title: string

  @IsString()
  @MaxLength(255)
  @IsOptional()
  description?: string | null | undefined

  @IsNumber()
  @IsPositive()
  @Type(() => Number)
  price: number
}
