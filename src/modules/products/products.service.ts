import { Injectable } from '@nestjs/common'
import type { Product } from '@prisma/client'

import type { PaginationDto } from '../../core/dtos/pagination.dto'
import { PrismaService } from '../../core/services/prisma.service'
import { createPaginatedResponse } from '../../core/utils/pagination'
import { UploadImageService } from '../upload-image/upload-image.service'
import type { CreateProductDto } from './dto/create-product.dto'
import type { UpdateProductDto } from './dto/update-product.dto'

@Injectable()
export class ProductsService {
  constructor(
    private readonly prismaService: PrismaService,
    private readonly uploadImageService: UploadImageService,
  ) {}

  public async create(
    imageFile: Express.Multer.File,
    createProductDto: CreateProductDto,
  ) {
    const image = await this.uploadImageService.upload(imageFile)
    return this.prismaService.product.create({
      data: { ...createProductDto, image },
    })
  }

  public async findAll(paginationDto: PaginationDto) {
    return createPaginatedResponse<Product>({
      db: this.prismaService.product,
      paginationDto,
    })
  }

  public async findOne(id: string) {
    return this.prismaService.product.findUnique({ where: { id } })
  }

  public async update(
    id: string,
    updateProductDto: UpdateProductDto,
    file?: Express.Multer.File,
  ) {
    const image = file ? await this.uploadImageService.upload(file) : undefined
    return this.prismaService.product.update({
      where: { id },
      data: { ...updateProductDto, image },
    })
  }

  public async remove(id: string) {
    return this.prismaService.product.delete({ where: { id } })
  }
}
