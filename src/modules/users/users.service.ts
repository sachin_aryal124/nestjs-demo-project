import { Injectable } from '@nestjs/common'
import type { User } from '@prisma/client'
import * as argon2 from 'argon2'

import { PrismaService } from '../../core/services/prisma.service'
import type { CreateUserDto } from './dtos/create-user-dto'
import type { UpdateUserDto } from './dtos/update-user.dto'

@Injectable()
export class UsersService {
  constructor(private readonly prisma: PrismaService) {}

  public async create(createUserDto: CreateUserDto): Promise<User> {
    const hashedPassword = await argon2.hash(createUserDto.password)
    return this.prisma.user.create({
      data: { ...createUserDto, password: hashedPassword },
    })
  }

  public async findAll(): Promise<User[]> {
    return this.prisma.user.findMany()
  }

  public async findOne(email: string): Promise<User | null> {
    return this.prisma.user.findUnique({ where: { email } })
  }

  public async update(email: string, updateUserDto: UpdateUserDto) {
    return this.prisma.user.update({
      where: { email },
      data: updateUserDto,
    })
  }
  public async delete(email: string) {
    return this.prisma.user.delete({ where: { email } })
  }
}
