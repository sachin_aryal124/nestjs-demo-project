import type { Prisma } from '@prisma/client'
import { IsEmail, IsOptional, MinLength } from 'class-validator'

export class UpdateUserDto implements Prisma.UserUpdateInput {
  @MinLength(3)
  @IsOptional()
  firstName?: string

  @MinLength(3)
  @IsOptional()
  lastName?: string

  @IsEmail()
  @IsOptional()
  email?: string

  @MinLength(8)
  @IsOptional()
  password?: string

  @MinLength(32)
  @IsOptional()
  passwordResetToken?: string

  @IsOptional()
  passwordResetTokenExpiresAt?: Date

  @IsOptional()
  deletedAt?: Date
}
