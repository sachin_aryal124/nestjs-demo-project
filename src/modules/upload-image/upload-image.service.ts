import type { S3ClientConfig } from '@aws-sdk/client-s3'
import { PutObjectCommand, S3Client } from '@aws-sdk/client-s3'
import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { v4 as uuidV4 } from 'uuid'

@Injectable()
export class UploadImageService {
  private readonly config: S3ClientConfig = {
    forcePathStyle: true,
    region: this.configService.getOrThrow('AWS_S3_REGION'),
    credentials: {
      accessKeyId: this.configService.getOrThrow('AWS_ACCESS_KEY_ID'),
      secretAccessKey: this.configService.getOrThrow('AWS_SECRET_ACCESS_KEY'),
    },
    endpoint: this.configService.getOrThrow('AWS_S3_ENDPOINT'),
  }
  private readonly s3Client = new S3Client(this.config)
  constructor(private readonly configService: ConfigService) {}

  private getUploadedImageUrl(fileKey: string) {
    const bucketName =
      this.configService.getOrThrow<string>('AWS_S3_BUCKET_NAME')
    const endpoint = this.configService.getOrThrow<string>('AWS_S3_ENDPOINT')
    return `${endpoint}/${bucketName}/${fileKey}`
  }

  public async upload(imageFile: Express.Multer.File) {
    const fileKey = uuidV4()
    await this.s3Client.send(
      new PutObjectCommand({
        Bucket: this.configService.getOrThrow('AWS_S3_BUCKET_NAME'),
        Key: fileKey,
        Body: imageFile.buffer,
        ContentType: imageFile.mimetype,
        ACL: 'public-read',
      }),
    )
    return this.getUploadedImageUrl(fileKey)
  }
}
