import { Injectable } from '@nestjs/common'
import type { Order, User } from '@prisma/client'

import type { PaginationDto } from '../../core/dtos/pagination.dto'
import { PrismaService } from '../../core/services/prisma.service'
import { createPaginatedResponse } from '../../core/utils/pagination'
import type { CreateOrderDto } from './dto/create-order.dto'
import type { UpdateOrderDto } from './dto/update-order.dto'

@Injectable()
export class OrdersService {
  constructor(private readonly prismaService: PrismaService) {}
  public async create(createOrderDto: CreateOrderDto, user: User) {
    const { productId, totalPrice, totalQuantity, orderStatus } = createOrderDto
    return this.prismaService.order.create({
      data: {
        totalPrice,
        totalQuantity,
        orderStatus,
        product: {
          connect: {
            id: productId,
          },
        },
        user: {
          connect: {
            id: user.id,
          },
        },
      },
    })
  }

  public async findAll(paginationDto: PaginationDto, user: User) {
    return createPaginatedResponse<Order>({
      db: this.prismaService.order,
      paginationDto,
      where: { userId: user.id },
    })
  }

  public async findOne(id: string, user: User) {
    return this.prismaService.order.findFirst({
      where: { id, userId: user.id },
    })
  }

  public async update(id: string, updateOrderDto: UpdateOrderDto, user: User) {
    return this.prismaService.order.updateMany({
      where: { id, userId: user.id },
      data: updateOrderDto,
    })
  }

  public async remove(id: string, user: User) {
    return this.prismaService.order.deleteMany({
      where: { id, userId: user.id },
    })
  }
}
