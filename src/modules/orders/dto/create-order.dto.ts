import type { Prisma } from '@prisma/client'
import { OrderStatus } from '@prisma/client'
import { IsEnum, IsNumber, IsOptional, IsUUID } from 'class-validator'

export class CreateOrderDto
  implements Omit<Prisma.OrderUncheckedCreateInput, 'userId'>
{
  @IsUUID()
  productId: string

  @IsNumber()
  totalQuantity: number

  @IsNumber()
  totalPrice: number

  @IsEnum(OrderStatus)
  @IsOptional()
  orderStatus: OrderStatus
}
