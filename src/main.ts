import { ValidationPipe } from '@nestjs/common'
import { NestFactory } from '@nestjs/core'
import * as cookieParser from 'cookie-parser'
import helmet from 'helmet'
import { Logger } from 'nestjs-pino'

import { AppModule } from './app.module'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  app.useLogger(app.get(Logger))
  app.setGlobalPrefix('/api/v1')
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    }),
  )
  // register additional third party middlewares
  // Parse Cookie header and populate req.cookies with an object keyed by the cookie names
  app.use(cookieParser())
  // Helmet helps you secure your Express apps by setting various HTTP headers
  app.use(helmet())
  await app.listen(3000)
}
bootstrap()
