import { Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { MailerModule } from '@nestjs-modules/mailer'
import { LoggerModule } from 'nestjs-pino'

import { AuthModule } from './modules/auth/auth.module'
import { OrdersModule } from './modules/orders/orders.module'
import { ProductsModule } from './modules/products/products.module'
import { UploadImageModule } from './modules/upload-image/upload-image.module'
import { UsersModule } from './modules/users/users.module'

@Module({
  imports: [
    AuthModule,
    UsersModule,
    LoggerModule.forRoot({
      pinoHttp: {
        transport: {
          target: 'pino-pretty',
          options: {
            singleLine: true,
          },
        },
      },
    }),
    MailerModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        transport: configService.get<string>('MAIL_TRANSPORT'),
        defaults: {
          from: configService.get<string>('MAIL_FROM'),
        },
      }),
    }),
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    OrdersModule,
    ProductsModule,
    UploadImageModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
