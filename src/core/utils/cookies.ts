import type { CookieOptions, Response } from 'express'

import type { AuthToken } from '../../modules/auth/auth.service'
import {
  ACCESS_TOKEN_COOKIE_NAME,
  REFRESH_TOKEN_COOKIE_NAME,
} from '../constants/strings'

export const setResponseCookies = (
  name: string,
  value: string,
  options: CookieOptions = {},
) => {
  return (response: Response) => {
    response.cookie(name, value, options)
  }
}

export const clearResponseCookies = (name: string) => {
  return (response: Response) => {
    response.clearCookie(name)
  }
}

export const setAccessAndRefreshTokenCookie = (token: AuthToken) => {
  return (response: Response) => {
    const options: CookieOptions = {
      httpOnly: true,
      secure: process.env.NODE_ENV === 'production',
    }
    setResponseCookies(
      ACCESS_TOKEN_COOKIE_NAME,
      token.accessToken,
      options,
    )(response)
    setResponseCookies(
      REFRESH_TOKEN_COOKIE_NAME,
      token.refreshToken,
      options,
    )(response)
  }
}
export const clearAccessAndRefreshTokenCookie = (response: Response) => {
  response.clearCookie(ACCESS_TOKEN_COOKIE_NAME)
  response.clearCookie(REFRESH_TOKEN_COOKIE_NAME)
}
