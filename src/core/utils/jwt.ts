import type { Request } from 'express'

export const extractJwtFromCookie = (cookieName: string) => {
  return (request: Request): string | null => {
    const token = request.cookies?.[cookieName]
    return token ?? null
  }
}
