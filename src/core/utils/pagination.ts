import type { Prisma } from '@prisma/client'

import type { PaginationDto } from '../dtos/pagination.dto'

export interface CreatePaginationOptions {
  totalItems: number
  page?: number
  perPage?: number
}
export interface FindManyFilters<TItem> {
  where?: Partial<TItem>
  skip?: number
  take?: number
}
export interface PaginationDB<TItem> {
  count: () => Prisma.PrismaPromise<number>
  findMany: (filters: FindManyFilters<TItem>) => Prisma.PrismaPromise<TItem[]>
}
export interface CreatePaginatedResponseFnParams<TItem, TDB> {
  db: TDB
  where?: Partial<TItem>
  paginationDto: PaginationDto
}
export interface CreatePaginatedResponseResult<TItem> {
  totalPages: number
  nextPage: number | null
  previousPage: number | null
  data: TItem[]
}

export const createPaginationData = (options: CreatePaginationOptions) => {
  const { totalItems, page = 1, perPage = 10 } = options
  const totalPages = Math.ceil(totalItems / perPage)
  const skip = (page - 1) * perPage
  const nextPage = page < totalPages ? page + 1 : null
  const previousPage = page > 1 ? page - 1 : null
  return {
    totalPages,
    skip,
    nextPage,
    previousPage,
    take: perPage,
  }
}
export const createPaginatedResponse = async <
  TItem,
  TDB extends PaginationDB<TItem> = any,
>({
  db,
  where,
  paginationDto,
}: CreatePaginatedResponseFnParams<TItem, TDB>): Promise<
  CreatePaginatedResponseResult<TItem>
> => {
  const totalItems = await db.count()
  const { page, perPage } = paginationDto
  const { skip, take, nextPage, previousPage, totalPages } =
    createPaginationData({
      totalItems,
      page,
      perPage,
    })
  const data = await db.findMany({ skip, take, where: where })
  return {
    totalPages,
    nextPage,
    previousPage,
    data,
  }
}
