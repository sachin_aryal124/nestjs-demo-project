/*
  Warnings:

  - Changed the type of `order_id` on the `OrderItem` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.

*/
-- AlterTable
ALTER TABLE "OrderItem" DROP COLUMN "order_id",
ADD COLUMN     "order_id" INTEGER NOT NULL;
