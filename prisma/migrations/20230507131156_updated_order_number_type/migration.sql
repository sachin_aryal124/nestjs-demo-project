/*
  Warnings:

  - Changed the type of `order_number` on the `Order` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.

*/
-- AlterTable
ALTER TABLE "Order" DROP COLUMN "order_number",
ADD COLUMN     "order_number" INTEGER NOT NULL;
