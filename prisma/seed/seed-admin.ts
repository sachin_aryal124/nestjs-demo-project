import { stdin as input, stdout as output } from 'node:process'

import { PrismaClient } from '@prisma/client'
import argon2 from 'argon2'
import nodeReadLine from 'readline'
import z from 'zod'

const QuestionSchema = z.object({
  firstName: z.string().nonempty(),
  lastName: z.string().nonempty(),
  email: z.string().email(),
  password: z.string().nonempty(),
})

const askQuestion = (question: string): Promise<string> => {
  const readline = nodeReadLine.createInterface({
    input,
    output,
  })
  return new Promise<string>(resolve => {
    readline.question(question, data => {
      resolve(data)
      readline.close()
    })
  })
}

async function seedAdmin(): Promise<void> {
  const firstName = await askQuestion('Admin first name ?\n')
  const lastName = await askQuestion('Admin last name ?\n')
  const email = await askQuestion('Admin email ?\n')
  const password = await askQuestion('Admin password ?\n')
  const hashedPassword = await argon2.hash(password)
  const adminData = { firstName, lastName, email, password: hashedPassword }
  const validatedAdminData = await QuestionSchema.parseAsync(adminData)
  const prisma = new PrismaClient()
  const adminUser = await prisma.user.create({
    data: {
      ...validatedAdminData,
      role: 'ADMIN',
    },
  })
  console.log('Admin user created: ', adminUser)
}
seedAdmin()
  .catch(error => {
    if (error instanceof z.ZodError) {
      console.error(error.issues)
      return
    }
    if (error.code === 'P2002') {
      console.error('Email already exists')
      return
    }
    console.error(error)
  })
  .finally(() => {
    process.exit(0)
  })
