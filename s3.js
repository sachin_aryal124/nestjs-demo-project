/* eslint-disable @typescript-eslint/no-var-requires */
const fs = require('fs')
const S3rver = require('s3rver')

new S3rver({
  port: 5000,
  directory: './s3',
  configureBuckets: [
    {
      name: 'auth-demo-project',
      configs: [fs.readFileSync('./cors.xml')],
    },
  ],
}).run((error, address) => {
  if (error) {
    console.error(error)
    process.exit(1)
  }
  console.log('S3 server listening on', address)
})
